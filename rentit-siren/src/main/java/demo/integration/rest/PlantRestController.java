package demo.integration.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.code.siren4j.component.Entity;
import com.google.code.siren4j.converter.ReflectingConverter;
import com.google.code.siren4j.converter.ResourceConverter;
import com.google.code.siren4j.error.Siren4JException;
import com.google.code.siren4j.resource.CollectionResource;

import demo.integration.dto.PlantResource;
import demo.integration.dto.PlantResourceAssembler;
import demo.models.Plant;
import demo.models.PlantRepository;

@RestController
@RequestMapping("/rest/plants")
public class PlantRestController {

	@Autowired
	PlantRepository repo;
	PlantResourceAssembler assembler = new PlantResourceAssembler();
		
	@RequestMapping(value="", method=RequestMethod.GET)
	public String getAllPlants() throws Siren4JException {
		CollectionResource<PlantResource> result = new CollectionResource<PlantResource>();
		for (Plant p: repo.findAll())
			result.add(assembler.toResource(p));
		ResourceConverter converter = ReflectingConverter.newInstance();
		Entity entity = converter.toEntity(result);
		return entity.toString();
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public String getPlant(@PathVariable Long id) throws Siren4JException {
		PlantResource res = assembler.toResource(repo.findOne(id));
		ResourceConverter converter = ReflectingConverter.newInstance();
		Entity entity = converter.toEntity(res);
		return entity.toString();
	}
}
