package demo.integration;

import java.util.List;

import demo.integration.dto.PlantResource;

public interface RequestGateway {
	List<PlantResource> getAllPlants();
	PlantResource getPlant(Long id);
}